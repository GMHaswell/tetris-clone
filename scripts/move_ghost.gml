with (obj_ghost_block) {instance_destroy();}
if(is_undefined(active_piece)) {
    if(!is_undefined(ghost)) {
        instance_destroy(ghost);
        ghost = undefined;
    }
    return 0;
}
if(!is_undefined(ghost)) {
    instance_destroy(ghost);
    ghost = undefined;
}
ghost = instance_create(x, y, obj_piece);
ghost.is_ghost = true;
ghost.set = false;
ghost.color = active_piece.color;
ds_list_copy(ghost.spots, active_piece.spots);

while(move_piece(ghost, 0, 1) == 1) {

}

for(var i = 0; i < ds_list_size(ghost.spots); i++) {
    if(ghost.spots[| i] != active_piece.spots[| i]) {
        ghost.spots[| i].block = instance_create(ghost.spots[| i].x, ghost.spots[| i].y, obj_ghost_block);
    }
}

draw_blocks(active_piece);
