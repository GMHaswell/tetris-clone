var tmp_queue = ds_queue_create();
var i = 0;
while(!ds_queue_empty(piece_queue)) {
    var tmp_p = ds_queue_dequeue(piece_queue);
    var cur_b = preview_boards[| i].board;
    ds_queue_enqueue(tmp_queue, tmp_p);
    
    clear_board(cur_b);
    
    for(var j = 0; j < 4; j++) {
        tmp_p.spots[| j] = cur_b[tmp_p.spots[| j].col, tmp_p.spots[| j].row];
    }
    draw_blocks(tmp_p);
    i++;
}
ds_queue_copy(piece_queue, tmp_queue);
