var blocks = argument[0];
var origin = blocks[| 0];
var tests = 0;

for(var i = 0; i < 4; i++) {
    var cur_block = blocks[| i];
    if(!is_undefined(cur_block.block) && ds_list_find_index(active_piece.spots, cur_block) == -1) {
        var dx = origin.col - cur_block.col;
        for(var j = 0; j < 4; j++) {
            ds_list_replace(blocks, j, board[blocks[| j].col + dx, blocks[| j].row]);
        }
        origin = blocks[| 0];
        i = 0;
        tests++;
    }
    if(tests > 3) {
        return 0;
    }
}

return 1;
