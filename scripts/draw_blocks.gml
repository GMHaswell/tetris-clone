var piece = argument[0]

for(var i = 0; i < ds_list_size(piece.spots); i++) {
    var p = piece.spots[| i];
    if(piece.is_ghost) {
        p.block = instance_create(p.x, p.y, obj_ghost_block);
    } else {
        p.block = instance_create(p.x, p.y, obj_block);
    }
    p.block.draw_color = piece.color;
}
