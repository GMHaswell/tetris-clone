var piece = argument[0];
var x_move = argument[1];
var y_move = argument[2];

if(is_undefined(piece)) {
    return 0;
}

for(var i = 0; i < 4; i++) {
    var try_col = piece.spots[| i].col + x_move;
    var try_row = piece.spots[| i].row + y_move;
    var try_spot = board[try_col, try_row];

    if(ds_list_find_index(piece.spots, try_spot) == -1) {
        if(!is_undefined(try_spot.block) && object_get_name(try_spot.block.object_index) == "obj_block") {
            if(x_move != 0) {
                return -1;
            } else if(y_move == 1) {
                if(piece == active_piece) {
                    instance_destroy(ghost);
                    ghost = undefined;
                    draw_blocks(active_piece);
                    clear_lines();
                    instance_destroy(active_piece);
                    active_piece = undefined;
                }
                return 0;
            } else if(y_move > 1) {
                y_move--;
                i = 0;
            }
        }
    }
}

var new_p = ds_list_create();
for(var i = 0; i < 4; i++) {
    var col = piece.spots[| i].col;
    var row = piece.spots[| i].row;
    ds_list_add(new_p, board[col + x_move, row + y_move]);
    if(!is_undefined(piece.spots[| i].block)) {
        instance_destroy(piece.spots[| i].block);
        piece.spots[| i].block = undefined;
    }
}

ds_list_copy(piece.spots, new_p);

draw_blocks(piece);

return 1;
