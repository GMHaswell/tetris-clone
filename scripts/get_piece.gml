var type = argument[0];
var p = 0;
var c = 0;
var ret_piece = instance_create(x, y, obj_piece);

ret_piece.type = type;

switch(type) {
    //O
    case 0:
        p[0] = "10";
        p[1] = "11";
        p[2] = "20";
        p[3] = "21";
        c = c_yellow;
        break;
    //I
    case 1:
        p[0] = "10";
        p[1] = "20";
        p[2] = "00";
        p[3] = "30";
        c = c_aqua;
        break;
    //L
    case 2:
        p[0] = "10";
        p[1] = "20";
        p[2] = "00";
        p[3] = "01";
        c = c_orange;
        break;
    //J
    case 3:
        p[0] = "10";
        p[1] = "20";
        p[2] = "00";
        p[3] = "21";
        c = c_blue;
        break;
    //S
    case 4:
        p[0] = "11";
        p[1] = "20";
        p[2] = "01";
        p[3] = "10";
        c = c_green;
        break;
    //Z
    case 5:
        p[0] = "11";
        p[1] = "10";
        p[2] = "00";
        p[3] = "21";
        c = c_red;
        break;
    //T
    case 6:
        p[0] = "11";
        p[1] = "01";
        p[2] = "10";
        p[3] = "21";
        c = c_purple;
        break;
}

ret_piece.color = c;

for(var i = 0; i < 4; i++) {
    var sp = board[real(string_char_at(p[i], 1)), real(string_char_at(p[i], 2))]; 
    ds_list_add(ret_piece.spots, sp);
}

return ret_piece;
