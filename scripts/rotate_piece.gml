var piece = argument[0];
var dir = argument[1];
var new_pos = ds_list_create();
var n_color = piece.color;
var origin = piece.spots[| 0];

switch(piece.type) {
    //O
    case 0:
        return 0;
    //I
    case 1:
        if(piece.rotation == 0) {
            ds_list_add(new_pos, board[origin.col, origin.row]);
            ds_list_add(new_pos, board[origin.col, origin.row + 1]);
            ds_list_add(new_pos, board[origin.col, origin.row - 1]);
            ds_list_add(new_pos, board[origin.col, origin.row + 2]);
            piece.rotation++;
        } else if (piece.rotation == 1) {
            ds_list_add(new_pos, board[origin.col, origin.row]);
            ds_list_add(new_pos, board[origin.col + 1, origin.row]);
            ds_list_add(new_pos, board[origin.col - 1, origin.row]);
            ds_list_add(new_pos, board[origin.col + 2, origin.row]);
            piece.rotation = 0;
        }
        break;
    //L
    case 2:
        if(piece.rotation == 0) {
            ds_list_add(new_pos, board[origin.col, origin.row]);
            ds_list_add(new_pos, board[origin.col, origin.row + 1]);
            ds_list_add(new_pos, board[origin.col, origin.row - 1]);
            ds_list_add(new_pos, board[origin.col + 1, origin.row + 1]);
            piece.rotation++;
        } else if(piece.rotation == 1) {
            ds_list_add(new_pos, board[origin.col, origin.row]);
            ds_list_add(new_pos, board[origin.col - 1, origin.row]);
            ds_list_add(new_pos, board[origin.col + 1, origin.row]);
            ds_list_add(new_pos, board[origin.col + 1, origin.row - 1]);
            piece.rotation++;
        } else if(piece.rotation == 2) {
            ds_list_add(new_pos, board[origin.col, origin.row]);
            ds_list_add(new_pos, board[origin.col - 1, origin.row - 1]);
            ds_list_add(new_pos, board[origin.col, origin.row - 1]);
            ds_list_add(new_pos, board[origin.col, origin.row + 1]);
            piece.rotation++;
        } else if (piece.rotation == 3) {
            ds_list_add(new_pos, board[origin.col, origin.row]);
            ds_list_add(new_pos, board[origin.col - 1, origin.row]);
            ds_list_add(new_pos, board[origin.col + 1, origin.row]);
            ds_list_add(new_pos, board[origin.col - 1, origin.row + 1]);
            piece.rotation = 0;
        }
        break;
    //J
    case 3:
        if(piece.rotation == 0) {
            ds_list_add(new_pos, board[origin.col, origin.row]);
            ds_list_add(new_pos, board[origin.col + 1, origin.row - 1]);
            ds_list_add(new_pos, board[origin.col, origin.row - 1]);
            ds_list_add(new_pos, board[origin.col, origin.row + 1]);
            piece.rotation++;
        } else if(piece.rotation == 1) {
            ds_list_add(new_pos, board[origin.col, origin.row]);
            ds_list_add(new_pos, board[origin.col - 1, origin.row]);
            ds_list_add(new_pos, board[origin.col + 1, origin.row]);
            ds_list_add(new_pos, board[origin.col - 1, origin.row - 1]);
            piece.rotation++;
        } else if(piece.rotation == 2) {
            ds_list_add(new_pos, board[origin.col, origin.row]);
            ds_list_add(new_pos, board[origin.col, origin.row + 1]);
            ds_list_add(new_pos, board[origin.col, origin.row - 1]);
            ds_list_add(new_pos, board[origin.col - 1, origin.row + 1]);
            piece.rotation++;
        } else if (piece.rotation == 3) {
            ds_list_add(new_pos, board[origin.col, origin.row]);
            ds_list_add(new_pos, board[origin.col - 1, origin.row]);
            ds_list_add(new_pos, board[origin.col + 1, origin.row]);
            ds_list_add(new_pos, board[origin.col + 1, origin.row + 1]);
            piece.rotation = 0;
        }
        break;
    //S
    case 4:
        if(piece.rotation == 0) {
            ds_list_add(new_pos, board[origin.col, origin.row]);
            ds_list_add(new_pos, board[origin.col, origin.row - 1]);
            ds_list_add(new_pos, board[origin.col + 1, origin.row]);
            ds_list_add(new_pos, board[origin.col + 1, origin.row + 1]);
            piece.rotation++;
        } else if (piece.rotation == 1) {
            ds_list_add(new_pos, board[origin.col, origin.row]);
            ds_list_add(new_pos, board[origin.col - 1, origin.row]);
            ds_list_add(new_pos, board[origin.col, origin.row - 1]);
            ds_list_add(new_pos, board[origin.col + 1, origin.row - 1]);
            piece.rotation = 0;
        }
        break;
    //Z
    case 5:
        if(piece.rotation == 0) {
            ds_list_add(new_pos, board[origin.col, origin.row]);
            ds_list_add(new_pos, board[origin.col + 1, origin.row]);
            ds_list_add(new_pos, board[origin.col + 1, origin.row - 1]);
            ds_list_add(new_pos, board[origin.col, origin.row + 1]);
            piece.rotation++;
        } else if (piece.rotation == 1) {
            ds_list_add(new_pos, board[origin.col, origin.row]);
            ds_list_add(new_pos, board[origin.col, origin.row - 1]);
            ds_list_add(new_pos, board[origin.col - 1, origin.row - 1]);
            ds_list_add(new_pos, board[origin.col + 1, origin.row]);
            piece.rotation = 0;
        }
        break;
    //T
    case 6:
        if(piece.rotation == 0) {
            ds_list_add(new_pos, board[origin.col, origin.row]);
            ds_list_add(new_pos, board[origin.col - 1, origin.row]);
            ds_list_add(new_pos, board[origin.col, origin.row - 1]);
            ds_list_add(new_pos, board[origin.col, origin.row + 1]);
            piece.rotation++;
        } else if(piece.rotation == 1) {
            ds_list_add(new_pos, board[origin.col, origin.row]);
            ds_list_add(new_pos, board[origin.col - 1, origin.row]);
            ds_list_add(new_pos, board[origin.col + 1, origin.row]);
            ds_list_add(new_pos, board[origin.col, origin.row + 1]);
            piece.rotation++;
        } else if(piece.rotation == 2) {
            ds_list_add(new_pos, board[origin.col, origin.row]);
            ds_list_add(new_pos, board[origin.col + 1, origin.row]);
            ds_list_add(new_pos, board[origin.col, origin.row - 1]);
            ds_list_add(new_pos, board[origin.col, origin.row + 1]);
            piece.rotation++;
        } else if (piece.rotation == 3) {
            ds_list_add(new_pos, board[origin.col, origin.row]);
            ds_list_add(new_pos, board[origin.col - 1, origin.row]);
            ds_list_add(new_pos, board[origin.col + 1, origin.row]);
            ds_list_add(new_pos, board[origin.col, origin.row - 1]);
            piece.rotation = 0;
        }
        break;
}

for(var i = 0; i < 4; i++) {
    instance_destroy(piece.spots[| i].block);
    piece.spots[| i].block = undefined;
}

if(test_position(new_pos) == 1) {
    ds_list_copy(piece.spots, new_pos);
    //move_ghost();
    draw_blocks(piece);
    return 1;
} else {
    return 0;
}
