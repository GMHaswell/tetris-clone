show_debug_message("ghost blocks: " + string(instance_number(obj_ghost_block)));

var min_row = global.BOARD_HEIGHT - global.BOARD_EXTRA;
var max_row = active_piece.spots[| 0].row;

for(var i = 0; i < 4; i++) {
    var cur_row = active_piece.spots[| i].row;
    if(cur_row > max_row) {
        max_row = cur_row;
    } else if(cur_row < min_row) {
        min_row = cur_row;
    }
}

var clear_list = ds_list_create();

for(var i = min_row; i <= max_row; i++) {
    for(var j = global.BOARD_EXTRA; j < global.BOARD_WIDTH + global.BOARD_EXTRA; j++) {
        if(is_undefined(board[j, i].block)) {
            break;
        }
    }
    if(j == global.BOARD_WIDTH + global.BOARD_EXTRA) {
        for(j = global.BOARD_EXTRA; j < global.BOARD_WIDTH + global.BOARD_EXTRA; j++) {
            instance_destroy(board[j, i].block);
            board[j, i].block = undefined;
        }
        ds_list_add(clear_list, i);
    }
}

if(ds_list_size(clear_list) == 0) {
    return 0;
}

lines += ds_list_size(clear_list);
t_score += ds_list_size(clear_list) * 1000;

if(lines >= level * 20) {
    level++;
    fall_speed = (60 - (5 * level));
}

with(score_display) {
    level = other.level;
    t_score = other.t_score;
    lines = other.lines;
}

ds_list_sort(clear_list, false);

var gap = 1;
var bottom_line = clear_list[| 0];

for(var i = clear_list[| 0] - 1; i > global.BOARD_EXTRA; i--) {
    var found_line = ds_list_find_index(clear_list, i);
    if(found_line != -1) {
        gap++;
        ds_list_delete(clear_list, found_line);
        continue;
    }
            
    for(var j = global.BOARD_EXTRA; j < global.BOARD_WIDTH + global.BOARD_EXTRA; j++) {
        if(!is_undefined(board[j, i].block)) {
            board[j, i + gap].block = instance_create(board[j, i + gap].x, board[j, i + gap].y, obj_block);
            board[j, i + gap].block.draw_color = board[j, i].block.draw_color;
            instance_destroy(board[j, i].block);
            board[j, i].block = undefined;
        }
    }    
}
