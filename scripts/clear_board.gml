var board = argument[0];

for(var i = 0; i < 4; i++) {
    for(var j = 0; j < 4; j++) {
        if(!is_undefined(board[i, j].block)) {
            instance_destroy(board[i, j].block);
            board[i, j].block = undefined;
        }
    }
}
