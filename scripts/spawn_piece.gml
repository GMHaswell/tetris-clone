active_piece = ds_queue_dequeue(piece_queue);
for(var i = 0; i < 4; i++) {
    active_piece.spots[| i] = board[global.SPAWN_COL + active_piece.spots[| i].col, global.SPAWN_ROW + active_piece.spots[| i].row];
}
draw_blocks(active_piece);

if(ds_list_empty(piece_bag)) {
    fill_piece_bag();
}
ds_queue_enqueue(piece_queue, piece_bag[| 0]);
ds_list_delete(piece_bag, 0);

update_previews();
